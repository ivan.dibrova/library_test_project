from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('library.urls', 'library'), namespace='library')),
]


urlpatterns += staticfiles_urlpatterns()

