from django.contrib import admin

from library.models import Book, Genre, Location, Subscription, BookMovement


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'publication_year', 'genre', 'location')


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('room', 'shelving', 'shelf')


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'birth_date', 'address', 'phone_number')


@admin.register(BookMovement)
class BookMovementAdmin(admin.ModelAdmin):
    list_display = ('book', 'subscription', 'date_getting', 'date_return', 'book_return_status', 'book_request')

