import json

from datetime import datetime

from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, JsonResponse, QueryDict

from library.models import Book, Genre, Location, Subscription, BookMovement
from library.forms import BookForm, GenreForm, LocationForm, SubscriptionForm, BookMovementForm

class MainPageView(View):
    """ Show Main Page (index.html) """

    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        book_requests = dict(BookMovement.BOOK_REQUEST_CHOICES)
        statistics_by_dates = [
            {
                'date': book_movement.date_getting if book_movement.book_request == BookMovement.BOOK_REQUEST_GETTING
                                                   else book_movement.date_return,
                'book': '{} - {}'.format(book_movement.book.author, book_movement.book.name),
                'subscription': book_movement.subscription.full_name,
                'request': book_requests.get(book_movement.book_request, BookMovement.BOOK_REQUEST_GETTING)
            }
            for book_movement in BookMovement.objects.all()
        ]
        statistics_by_dates.sort(key=lambda k: k['date'], reverse=True)

        expired_subscriptions = []

        for item in BookMovement.objects.filter(book_request=BookMovement.BOOK_REQUEST_GETTING):
            expired_days = (datetime.now().date() - item.date_return).days
            if expired_days > 0:
                return_condition = False
                return_items = BookMovement.objects.filter(book=item.book, subscription=item.subscription,
                                                           book_request=BookMovement.BOOK_REQUEST_RETURN)
                if return_items:
                    for return_item in return_items:
                        if return_item.date_return > item.date_getting:
                            return_condition = True

                if not return_condition:
                    expired_subscriptions.append(
                        {
                            'subscription': item.subscription.full_name,
                            'book': '{} - {}'.format(item.book.author, item.book.name),
                            'date_return': item.date_return,
                            'expired_days': expired_days
                        }
                    )

        return render(request, self.template_name, {'statistics_by_dates': statistics_by_dates,
                                                    'expired_subscriptions': expired_subscriptions})


class BooksPageView(View):
    """ Show Books Page (books.html) """

    template_name = 'books.html'

    def get(self, request, *args, **kwargs):
        books = []
        for book in Book.objects.all():
            books.append({
                'id': book.id,
                'name': book.name,
                'author': book.author,
                'publication_year': book.publication_year,
                'genre': book.genre.name,
                'location': 'Room: {}. Shelving: {}. Shelf: {}'.format(book.location.room, book.location.shelving,
                                                                       book.location.shelf),
            })

        return render(request, self.template_name, {'books': books})


class BookChangeModal(View):
    def get(self, request, *args, **kwargs):
        book_id = request.GET.get('book_id', None)
        genres = dict((genre.id, genre.name) for genre in Genre.objects.all())
        location_template = 'Room - {}, Shelving - {}, Shelf - {}'
        locations = dict((location.id, location_template.format(location.room, location.shelving, location.shelf))
                                       for location in Location.objects.all())
        ajax_response = {
            'data': {},
            'genres': genres,
            'locations': locations,
            'location_name': ''
        }

        try:
            if book_id:
                book = Book.objects.get(pk=book_id)
                form = BookForm(instance=book)
                ajax_response['data'] = dict(form.initial)
                ajax_response['location_name'] = location_template.format(book.location.room, book.location.shelving,
                                                                   book.location.shelf)
        except:
            pass

        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            valid = False
            book_id = request.POST.get('book_id', None)
            if book_id:
                book = Book.objects.get(pk=book_id)
                form = BookForm(request.POST, instance=book)
                if form.is_valid():
                    form.save()
                    valid = True
            else:
                form = BookForm(request.POST)
                if form.is_valid():
                    book = form.save(commit=False)
                    book.save()
                    valid = True

            if valid:
                return HttpResponse(json.dumps({'success': True}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

    def delete(self, request, *args, **kwargs):
        try:
            book_id = QueryDict(request.body).get('book_id', None)
            if book_id:
                book = Book.objects.get(pk=book_id)
                book.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        except Exception:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')


class GenresPageView(View):
    """ Show Genres Page (genres.html) """

    template_name = 'genres.html'

    def get(self, request, *args, **kwargs):
        genres = []
        for genre in Genre.objects.all():
            genres.append({
                'id': genre.id,
                'name': genre.name,
            })

        return render(request, self.template_name, {'genres': genres})


class GenreChangeModal(View):
    def get(self, request, *args, **kwargs):
        genre_id = request.GET.get('genre_id', None)
        ajax_response = {}

        try:
            if genre_id:
                genre = Genre.objects.get(pk=genre_id)
                ajax_response = {
                    'genre_id': genre_id,
                    'genre_name': genre.name,
                }
        except:
            pass

        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            valid = False
            genre_id = request.POST.get('genre_id', None)
            if genre_id:
                genre = Genre.objects.get(pk=genre_id)
                form = GenreForm(request.POST, instance=genre)
                if form.is_valid():
                    form.save()
                    valid = True
            else:
                form = GenreForm(request.POST)
                if form.is_valid():
                    genre = form.save(commit=False)
                    genre.save()
                    valid = True
            if valid:
                return HttpResponse(json.dumps({'success': True}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

    def delete(self, request, *args, **kwargs):
        try:
            genre_id = QueryDict(request.body).get('genre_id', None)
            if genre_id:
                genre = Genre.objects.get(pk=genre_id)
                genre.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        except Exception:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')


class LocationsPageView(View):
    """ Show Locations Page (locations.html) """

    template_name = 'locations.html'

    def get(self, request, *args, **kwargs):
        locations = []
        for location in Location.objects.all():
            locations.append({
                'id': location.id,
                'room': location.room,
                'shelving': location.shelving,
                'shelf': location.shelf,
            })

        return render(request, self.template_name, {'locations': locations})


class LocationChangeModal(View):
    def get(self, request, *args, **kwargs):
        location_id = request.GET.get('location_id', None)
        ajax_response = {}

        try:
            if location_id:
                location = Location.objects.get(pk=location_id)
                ajax_response = {
                    'location_id': location_id,
                    'location_room': location.room,
                    'location_shelving': location.shelving,
                    'location_shelf': location.shelf,
                }
        except:
            pass

        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            valid = False
            location_id = request.POST.get('location_id', None)
            if location_id:
                location = Location.objects.get(pk=location_id)
                form = LocationForm(request.POST, instance=location)
                if form.is_valid():
                    form.save()
                    valid = True
            else:
                form = LocationForm(request.POST)
                if form.is_valid():
                    location = form.save(commit=False)
                    location.save()
                    valid = True
            if valid:
                return HttpResponse(json.dumps({'success': True}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

    def delete(self, request, *args, **kwargs):
        try:
            location_id = QueryDict(request.body).get('location_id', None)
            if location_id:
                location = Location.objects.get(pk=location_id)
                location.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        except Exception:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')


class SubscriptionsPageView(View):
    """ Show Subscriptions Page (subscriptions.html) """

    template_name = 'subscriptions.html'

    def get(self, request, *args, **kwargs):
        subscriptions = []
        for subscription in Subscription.objects.all():
            subscriptions.append({
                'id': subscription.id,
                'full_name': subscription.full_name,
                'birth_date': subscription.birth_date,
                'address': subscription.address,
                'phone_number': subscription.phone_number
            })

        return render(request, self.template_name, {'subscriptions': subscriptions})


class SubscriptionChangeModal(View):
    def get(self, request, *args, **kwargs):
        subscription_id = request.GET.get('subscription_id', None)
        ajax_response = {}

        try:
            if subscription_id:
                subscription = Subscription.objects.get(pk=subscription_id)
                ajax_response = {
                    'subscription_id': subscription.id,
                    'full_name': subscription.full_name,
                    'birth_date': subscription.birth_date.isoformat(),
                    'address': subscription.address,
                    'phone_number': subscription.phone_number,
                }
        except:
            pass

        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            valid = False
            subscription_id = request.POST.get('subscription_id', None)
            if subscription_id:
                subscription = Subscription.objects.get(pk=subscription_id)
                form = SubscriptionForm(request.POST, instance=subscription)
                if form.is_valid():
                    form.save()
                    valid = True
            else:
                form = SubscriptionForm(request.POST)
                if form.is_valid():
                    subscription = form.save(commit=False)
                    subscription.save()
                    valid = True
            if valid:
                return HttpResponse(json.dumps({'success': True}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

    def delete(self, request, *args, **kwargs):
        try:
            subscription_id = QueryDict(request.body).get('subscription_id', None)
            if subscription_id:
                subscription = Subscription.objects.get(pk=subscription_id)
                subscription.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        except Exception:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

class BookMovementPageView(View):
    """ Show Book Movement Page (book_movement.html) """

    template_name = 'book_movement.html'

    def get(self, request, *args, **kwargs):
        book_movements = []
        for book_movement in BookMovement.objects.all():
            book_movements.append({
                'id': book_movement.id,
                'book': '{} - {}'.format(book_movement.book.author, book_movement.book.name),
                'subscription': book_movement.subscription.full_name,
                'date_getting': book_movement.date_getting,
                'date_return': book_movement.date_return,
                'book_return_status': dict(BookMovement.BOOK_RETURN_STATUS_CHOICES).get(
                                                book_movement.book_return_status, BookMovement.BOOK_RETURN_STATUS_GOOD),
                'book_request': dict(BookMovement.BOOK_REQUEST_CHOICES).get(book_movement.book_request,
                                                                            BookMovement.BOOK_REQUEST_GETTING)
            })

        return render(request, self.template_name, {'book_movements': book_movements})


class BookMovementChangeModal(View):
    def get(self, request, *args, **kwargs):
        book_movement_id = request.GET.get('book_movement_id', None)
        books = dict((book.id, '{} - {}'.format(book.author, book.name)) for book in Book.objects.all())
        subscriptions = dict((subscription.id, subscription.full_name) for subscription in Subscription.objects.all())
        book_return_statuses = dict(BookMovement.BOOK_RETURN_STATUS_CHOICES)
        book_requests = dict(BookMovement.BOOK_REQUEST_CHOICES)

        ajax_response = {
            'data': {},
            'books': books,
            'subscriptions': subscriptions,
            'book_return_statuses':book_return_statuses,
            'book_requests': book_requests,
            'book_id': '',
            'book': '',
            'subscription': '',
            'book_return_status': '',
            'book_request': ''
        }

        try:
            if book_movement_id:
                book_movement = BookMovement.objects.get(pk=book_movement_id)
                form = BookMovementForm(instance=book_movement)
                ajax_response['data'] = dict(form.initial)
                ajax_response['book'] = '{} - {}'.format(book_movement.book.author, book_movement.book.name)
                ajax_response['book_id'] = book_movement.book.id
                ajax_response['subscription'] = book_movement.subscription.full_name
                ajax_response['book_return_status'] = book_return_statuses.get(book_movement.book_return_status,
                                                                               BookMovement.BOOK_RETURN_STATUS_GOOD)
                ajax_response['book_request'] = book_requests.get(book_movement.book_request,
                                                                                BookMovement.BOOK_REQUEST_GETTING)
                ajax_response['data']['date_getting'] = ajax_response['data']['date_getting'].isoformat()
                ajax_response['data']['date_return'] = ajax_response['data']['date_return'].isoformat()
        except:
            pass

        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            valid = False
            book_movement_id = request.POST.get('book_movement_id', None)
            if book_movement_id:
                book_movement = BookMovement.objects.get(pk=book_movement_id)
                form = BookMovementForm(request.POST, instance=book_movement)
                if form.is_valid():
                    form.save()
                    valid = True
            else:
                form = BookMovementForm(request.POST)
                if form.is_valid():
                    book_movement = form.save(commit=False)
                    book_movement.save()
                    valid = True
            if valid:
                return HttpResponse(json.dumps({'success': True}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

    def delete(self, request, *args, **kwargs):
        try:
            book_movement_id = QueryDict(request.body).get('book_movement_id', None)
            if book_movement_id:
                book_movement = BookMovement.objects.get(pk=book_movement_id)
                book_movement.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        except Exception:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')

