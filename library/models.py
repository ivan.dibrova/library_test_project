from django.db import models


class Genre(models.Model):
    name = models.CharField(max_length=64, unique=True)


class Location(models.Model):
    room = models.PositiveIntegerField()
    shelving = models.PositiveIntegerField()
    shelf = models.PositiveIntegerField()

    class Meta():
        unique_together = ('room', 'shelving', 'shelf')


class Book(models.Model):
    name = models.CharField(max_length=128)
    author = models.CharField(max_length=128)
    publication_year = models.PositiveIntegerField()
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE, related_name='genre')
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='location')


class Subscription(models.Model):
    full_name = models.CharField(max_length=128)
    birth_date = models.DateField()
    address = models.CharField(max_length=256)
    phone_number = models.CharField(max_length=16)


class BookMovement(models.Model):
    BOOK_REQUEST_GETTING = 1
    BOOK_REQUEST_RETURN = 2

    BOOK_REQUEST_CHOICES = (
        (BOOK_REQUEST_GETTING, 'Book getting'),
        (BOOK_REQUEST_RETURN, 'Book return'),
    )

    BOOK_RETURN_STATUS_EXCELLENT = 1
    BOOK_RETURN_STATUS_GOOD = 2
    BOOK_RETURN_STATUS_FAIR = 3
    BOOK_RETURN_STATUS_POOR = 4

    BOOK_RETURN_STATUS_CHOICES = (
        (BOOK_RETURN_STATUS_EXCELLENT, 'Excellent'),
        (BOOK_RETURN_STATUS_GOOD, 'Good'),
        (BOOK_RETURN_STATUS_FAIR, 'Fair'),
        (BOOK_RETURN_STATUS_POOR, 'Poor'),
    )

    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='book')
    subscription = models.ForeignKey(Subscription, on_delete=models.CharField, related_name='subscription')
    date_getting = models.DateField()
    date_return = models.DateField()
    book_return_status = models.IntegerField(choices=BOOK_RETURN_STATUS_CHOICES, default=BOOK_RETURN_STATUS_GOOD)
    book_request = models.IntegerField(choices=BOOK_REQUEST_CHOICES, default=BOOK_REQUEST_GETTING)

