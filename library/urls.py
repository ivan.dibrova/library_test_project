from django.urls import path
from django.conf.urls import url
from library.views import MainPageView, BooksPageView, BookChangeModal, GenresPageView, GenreChangeModal, \
    LocationsPageView, LocationChangeModal, SubscriptionsPageView, SubscriptionChangeModal, \
    BookMovementPageView, BookMovementChangeModal

urlpatterns = [
    path('', MainPageView.as_view(), name='main_page'),
    path('books/', BooksPageView.as_view(), name='books'),
    path('genres/', GenresPageView.as_view(), name='genres'),
    path('locations/', LocationsPageView.as_view(), name='locations'),
    path('subscriptions/', SubscriptionsPageView.as_view(), name='subscriptions'),
    path('book_movement/', BookMovementPageView.as_view(), name='book_movement'),

    url(r'^ajax_book_change_modal/$', BookChangeModal.as_view(), name='ajax_book_change_modal'),
    url(r'^ajax_genre_change_modal/$', GenreChangeModal.as_view(), name='ajax_genre_change_modal'),
    url(r'^ajax_location_change_modal/$', LocationChangeModal.as_view(), name='ajax_location_change_modal'),
    url(r'^ajax_subscription_change_modal/$', SubscriptionChangeModal.as_view(), name='ajax_subscription_change_modal'),
    url(r'^ajax_book_movement_change_modal/$', BookMovementChangeModal.as_view(), name='ajax_book_movement_change_modal'),
]

