from django.forms import ModelForm

from library.models import Book, Genre, Location, Subscription, BookMovement


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ['name', 'author', 'publication_year', 'genre', 'location']

    def clean(self):
        form_data = self.cleaned_data
        return form_data


class GenreForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name']

    def clean(self):
        form_data = self.cleaned_data
        return form_data


class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = ['room', 'shelving', 'shelf']

    def clean(self):
        form_data = self.cleaned_data
        return form_data


class SubscriptionForm(ModelForm):
    class Meta:
        model = Subscription
        fields = ['full_name', 'birth_date', 'address', 'phone_number']

    def clean(self):
        form_data = self.cleaned_data
        return form_data


class BookMovementForm(ModelForm):
    class Meta:
        model = BookMovement
        fields = ['book', 'subscription', 'date_getting', 'date_return', 'book_return_status', 'book_request']

    def clean(self):
        form_data = self.cleaned_data
        return form_data

