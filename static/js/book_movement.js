$(document).on('ready', function () {
    $('#book_movements_modal').on('show.bs.modal', function (event) {
        showModalWindow(event);
    });

    $('#save_book_movement').click(function () {
        saveBookMovement();
    });

    $("#delete_book_movement").click(function () {
        deleteBookMovement(true, null);
    });

    $(".delete_book_movement").on('click', function(event) {
        deleteBookMovement(false, event);
    });

});

function showModalWindow(event) {
    let button = $(event.relatedTarget);
    let book_movement_id = button.data('book-movement-id');

    $('#book_movements_modal').find('div').removeClass('has-error');
    $('#book_movements_modal').find('input, select').val('');
    $('#book_movements_modal').find('span').text('');
    $('#book_movement_id').empty();
    $('#book').empty();
    $('#subscription').empty();
    $('#book_return_status').empty();
    $('#book_request').empty();

    if (book_movement_id !== null) {
        $('#delete_book_movement').show();

        let data = {
            'book_movement_id': book_movement_id,
        };

        $.ajax({
            type: "GET",
            url: links.ajax_book_movement_change_modal,
            data: data,
            success: function (data) {
                $('#book_movement_id').val(book_movement_id);
                booksSelect(data['books'], data['book_id']);
                subscriptionSelect(data['subscriptions'], data['subscription']);
                $('#date_getting').val(data['data']['date_getting']);
                $('#date_return').val(data['data']['date_return']);
                bookReturnStatusSelect(data['book_return_statuses'], data['book_return_status']);
                bookRequestSelect(data['book_requests'], data['book_request']);
            },
        });
    } else {
        $('#delete_book_movement').hide();
        $.ajax({
            type: "GET",
            url: links.ajax_book_movement_change_modal,
            success: function (data) {
                booksSelect(data['books'], data['book_id']);
                subscriptionSelect(data['subscriptions'], data['subscription']);
                bookReturnStatusSelect(data['book_return_statuses'], data['book_return_status']);
                bookRequestSelect(data['book_requests'], data['book_request']);
            },
        });
    }
}

function booksSelect(data, bookId) {
    if (data) {
        $.each(data, function(key, value) {
            if (bookId && key==bookId) {
                $('#book').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#book').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function subscriptionSelect(data, subscriptionName) {
    if (data) {
        $.each(data, function(key, value) {
            if (subscriptionName && value===subscriptionName) {
                $('#subscription').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#subscription').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function bookReturnStatusSelect(data, bookReturnStatus) {
    if (data) {
        $.each(data, function(key, value) {
            if (bookReturnStatus && value===bookReturnStatus) {
                $('#book_return_status').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#book_return_status').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function bookRequestSelect(data, bookRequest) {
    if (data) {
        $.each(data, function(key, value) {
            if (bookRequest && value===bookRequest) {
                $('#book_request').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#book_request').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function saveBookMovement() {
    let data = {
        'book_movement_id': $('#book_movement_id').val(),
        'book': $('#book option:selected').val(),
        'subscription': $('#subscription option:selected').val(),
        'date_getting': $('#date_getting').val(),
        'date_return': $('#date_return').val(),
        'book_return_status':$('#book_return_status option:selected').val(),
        'book_request':$('#book_request option:selected').val(),
    };
    let csrftoken = getCookie('csrftoken');

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        type: "POST",
        url: links.ajax_book_movement_change_modal,
        data: data,
        success: function (data) {
            if (!(data['success'])) {
                data['errors'].forEach(function (item, i, arr) {
                    $('#book_' + item[0] + '_div').addClass('has-error');
                    $('#book_' + item[0] + '_help').text(item[1]);
                });
            } else {
                $('#book_movements_modal').modal('hide');
                location.reload();
            }
        },
    });
}

function deleteBookMovement(bookMovementModal, event) {
    if (confirm("Are you sure, that you want delete this book movement?")) {
        let book_movement_id = null;

        if (bookMovementModal) {
            book_movement_id =  $('#book_movement_id').val()
        } else {
            let delete_btn = $(event.target);
            book_movement_id = delete_btn.attr('data-book-movement-id')
        }
        let data = {
            'book_movement_id': book_movement_id
        };
        let csrftoken = getCookie('csrftoken');

        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            type: "DELETE",
            url: links.ajax_book_movement_change_modal,
            data: data,
            success: function (data) {
                if (data['success']) {
                    if (bookMovementModal) {
                        $('#book_movements_modal').modal('hide');
                    }
                    location.reload();
                }
            },
        });
    }
}

