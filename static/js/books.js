$(document).on('ready', function () {
    $('#books_modal').on('show.bs.modal', function (event) {
        showModalWindow(event);
    });

    $('#save_book').click(function () {
        saveBook();
    });

    $("#delete_book").click(function () {
        deleteBook(true, null);
    });

    $(".delete_book").on('click', function(event) {
        deleteBook(false, event);
    });

});

function showModalWindow(event) {
    let button = $(event.relatedTarget);
    let book_id = button.data('book-id');

    $('#books_modal').find('div').removeClass('has-error');
    $('#books_modal').find('input, select').val('');
    $('#books_modal').find('span').text('');
    $('#book_id').empty();
    $('#book_name').empty();
    $('#author').empty();
    $('#publication_year').empty();
    $('#genre').empty();
    $('#location').empty();

    if (book_id !== null) {
        $('#delete_book').show();

        let data = {
            'book_id': book_id,
        };

        $.ajax({
            type: "GET",
            url: links.ajax_book_change_modal,
            data: data,
            success: function (data) {
                $('#book_id').val(book_id);
                $('#book_name').val(data['data']['name']);
                $('#author').val(data['data']['author']);
                $('#publication_year').val(data['data']['publication_year']);
                genresSelect(data['genres'], data['data']['genre']);
                locationsSelect(data['locations'], data['location_name']);
            },
        });
    } else {
        $('#delete_book').hide();
        $.ajax({
            type: "GET",
            url: links.ajax_book_change_modal,
            success: function (data) {
                genresSelect(data['genres'], null);
                locationsSelect(data['locations'], null);
            },
        });
    }
}

function genresSelect(data, genre_name) {
    if (data) {
        $.each(data, function(key, value) {
            if (genre_name && value===genre_name) {
                $('#genre').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#genre').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function locationsSelect(data, location_name) {
    if (data) {
        $.each(data, function(key, value) {
            if (location_name && value===location_name) {
                $('#location').append('<option value="'+key+'" selected>'+value+'</option>');
            } else {
                $('#location').append('<option value="'+key+'">'+value+'</option>');
            }
        });
    }
}

function saveBook() {
    let data = {
        'book_id': $('#book_id').val(),
        'name': $('#book_name').val(),
        'author': $('#author').val(),
        'publication_year': $('#publication_year').val(),
        'genre': $('#genre option:selected').val(),
        'location': $('#location option:selected').val(),
    };
    let csrftoken = getCookie('csrftoken');

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        type: "POST",
        url: links.ajax_book_change_modal,
        data: data,
        success: function (data) {
            if (!(data['success'])) {
                data['errors'].forEach(function (item, i, arr) {
                    $('#book_' + item[0] + '_div').addClass('has-error');
                    $('#book_' + item[0] + '_help').text(item[1]);
                });
            } else {
                $('#books_modal').modal('hide');
                location.reload();
            }
        },
    });
}

function deleteBook(book_modal, event) {
    if (confirm("Are you sure, that you want delete this book?")) {
        let book_id = null;

        if (book_modal) {
            book_id =  $('#book_id').val()
        } else {
            let delete_btn = $(event.target);
            book_id = delete_btn.attr('data-book-id')
        }

        let data = {
            'book_id': book_id
        };
        let csrftoken = getCookie('csrftoken');

        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            type: "DELETE",
            url: links.ajax_book_change_modal,
            data: data,
            success: function (data) {
                if (data['success']) {
                    if (book_modal) {
                        $('#books_modal').modal('hide');
                    }
                    location.reload();
                }
            },
        });
    }
}

