$(document).on('ready', function () {
    $('#subscriptions_modal').on('show.bs.modal', function (event) {
        showModalWindow(event);
    });

    $('#save_subscription').click(function () {
        saveSubscription();
    });

    $("#delete_subscription").click(function () {
        deleteSubscription(true, null);
    });

    $(".delete_subscription").on('click', function(event) {
        deleteSubscription(false, event);
    });
});

function showModalWindow(event) {
    let button = $(event.relatedTarget);
    let subscription_id = button.data('subscription-id');

    $('#subscriptions_modal').find('div').removeClass('has-error');
    $('#subscriptions_modal').find('input, select').val('');
    $('#subscriptions_modal').find('span').text('');
    $('#subscription_id').empty();
    $('#full_name').empty();
    $('#address').empty();
    $('#phone_number').empty();

    if (subscription_id !== null) {
        $('#delete_subscription').show();

        let data = {
            'subscription_id': subscription_id,
        };

        $.ajax({
            type: "GET",
            url: links.ajax_subscription_change_modal,
            data: data,
            success: function (data) {
                $('#subscription_id').val(data['subscription_id']);
                $('#full_name').val(data['full_name']);
                $('#birth_date').val(data['birth_date']);
                $('#address').val(data['address']);
                $('#phone_number').val(data['phone_number']);
            },
        });
    } else {
        $('#delete_subscription').hide();
    }
}

function saveSubscription() {
    let data = {
        'subscription_id': $('#subscription_id').val(),
        'full_name': $('#full_name').val(),
        'birth_date': $('#birth_date').val(),
        'address': $('#address').val(),
        'phone_number': $('#phone_number').val(),
    };
    let csrftoken = getCookie('csrftoken');

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        type: "POST",
        url: links.ajax_subscription_change_modal,
        data: data,
        success: function (data) {
            if (!(data['success'])) {
                data['errors'].forEach(function (item, i, arr) {
                    $('#' + item[0] + '_div').addClass('has-error');
                    $('#' + item[0] + '_help').text(item[1]);
                });
            } else {
                $('#subscriptions_modal').modal('hide');
                location.reload();
            }
        },
    });
}

function deleteSubscription(subscription_modal, event) {
    if (confirm("Are you sure, that you want delete this subscription?")) {
        let subscription_id = null;

        if (subscription_modal) {
            subscription_id =  $('#subscription_id').val()
        } else {
            let delete_btn = $(event.target);
            subscription_id = delete_btn.attr('data-subscription-id')
        }

        let data = {
            'subscription_id': subscription_id
        };
        let csrftoken = getCookie('csrftoken');

        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            type: "DELETE",
            url: links.ajax_subscription_change_modal,
            data: data,
            success: function (data) {
                if (data['success']) {
                    if (subscription_modal) {
                        $('#subscriptions_modal').modal('hide');
                    }
                    location.reload();
                }
            },
        });
    }
}