$(document).on('ready', function () {
    $('#locations_modal').on('show.bs.modal', function (event) {
        showModalWindow(event);
    });

    $('#save_location').click(function () {
        saveLocation();
    });

    $("#delete_location").click(function () {
        deleteLocation(true, null);
    });

    $(".delete_location").on('click', function(event) {
        deleteLocation(false, event);
    });
});

function showModalWindow(event) {
    let button = $(event.relatedTarget);
    let location_id = button.data('location-id');

    $('#locations_modal').find('div').removeClass('has-error');
    $('#locations_modal').find('input, select').val('');
    $('#locations_modal').find('span').text('');
    $('#location_id').empty();
    $('#location_room').empty();
    $('#location_shelving').empty();
    $('#location_shelf').empty();

    if (location_id !== null) {
        $('#delete_location').show();

        let data = {
            'location_id': location_id,
        };

        $.ajax({
            type: "GET",
            url: links.ajax_location_change_modal,
            data: data,
            success: function (data) {
                $('#location_id').val(data['location_id']);
                $('#location_room').val(data['location_room']);
                $('#location_shelving').val(data['location_shelving']);
                $('#location_shelf').val(data['location_shelf']);
            },
        });
    } else {
        $('#delete_location').hide();
    }
}

function saveLocation() {
    let data = {
        'location_id': $('#location_id').val(),
        'room': $('#location_room').val(),
        'shelving': $('#location_shelving').val(),
        'shelf': $('#location_shelf').val(),
    };
    let csrftoken = getCookie('csrftoken');

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        type: "POST",
        url: links.ajax_location_change_modal,
        data: data,
        success: function (data) {
            if (!(data['success'])) {
                data['errors'].forEach(function (item, i, arr) {
                    $('#location_' + item[0] + '_div').addClass('has-error');
                    $('#location_' + item[0] + '_help').text(item[1]);
                });
            } else {
                $('#locations_modal').modal('hide');
                location.reload();
            }
        },
    });
}

function deleteLocation(location_modal, event) {
    if (confirm("Are you sure, that you want delete this location?")) {
        let location_id = null;

        if (location_modal) {
            location_id =  $('#location_id').val()
        } else {
            let delete_btn = $(event.target);
            location_id = delete_btn.attr('data-location-id')
        }

        let data = {
            'location_id': location_id
        };
        let csrftoken = getCookie('csrftoken');

        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            type: "DELETE",
            url: links.ajax_location_change_modal,
            data: data,
            success: function (data) {
                if (data['success']) {
                    if (location_modal) {
                        $('#locations_modal').modal('hide');
                    }
                    location.reload();
                }
            },
        });
    }
}

