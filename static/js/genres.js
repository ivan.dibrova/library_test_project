$(document).on('ready', function () {
    $('#genres_modal').on('show.bs.modal', function (event) {
        showModalWindow(event);
    });

    $('#save_genre').click(function () {
        saveGenre();
    });

    $("#delete_genre").click(function () {
        deleteGenre(true, null);
    });

    $(".delete_genre").on('click', function(event) {
        deleteGenre(false, event);
    });
});

function showModalWindow(event) {
    let button = $(event.relatedTarget);
    let genre_id = button.data('genre-id');

    $('#genres_modal').find('div').removeClass('has-error');
    $('#genres_modal').find('input, select').val('');
    $('#genres_modal').find('span').text('');
    $('#genre_id').empty();
    $('#genre_name').empty();

    if (genre_id !== null) {
        $('#delete_genre').show();

        let data = {
            'genre_id': genre_id,
        };

        $.ajax({
            type: "GET",
            url: links.ajax_genre_change_modal,
            data: data,
            success: function (data) {
                $('#genre_id').val(data['genre_id']);
                $('#genre_name').val(data['genre_name']);
            },
        });
    } else {
        $('#delete_genre').hide();
    }
}

function saveGenre() {
    let data = {
        'genre_id': $('#genre_id').val(),
        'name': $('#genre_name').val(),
    };
    let csrftoken = getCookie('csrftoken');

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        type: "POST",
        url: links.ajax_genre_change_modal,
        data: data,
        success: function (data) {
            if (!(data['success'])) {
                data['errors'].forEach(function (item, i, arr) {
                    $('#genre_' + item[0] + '_div').addClass('has-error');
                    $('#genre_' + item[0] + '_help').text(item[1]);
                });
            } else {
                $('#genres_modal').modal('hide');
                location.reload();
            }
        },
    });
}

function deleteGenre(genre_modal, event) {
    if (confirm("Are you sure, that you want delete this genre?")) {
        let genre_id = null;

        if (genre_modal) {
            genre_id =  $('#genre_id').val()
        } else {
            let delete_btn = $(event.target);
            genre_id = delete_btn.attr('data-genre-id')
        }

        let data = {
            'genre_id': genre_id
        };
        let csrftoken = getCookie('csrftoken');

        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            type: "DELETE",
            url: links.ajax_genre_change_modal,
            data: data,
            success: function (data) {
                if (data['success']) {
                    if (genre_modal) {
                        $('#genres_modal').modal('hide');
                    }
                    location.reload();
                }
            },
        });
    }
}

